import React, { createContext, useEffect, useState } from 'react';
import axios from 'axios';

// Creamos el context
export const ModalContext = createContext();

const ModalProvider = (props) => {

    const [identificador, guardarIdentificador] = useState(null);
    // const [generar, guardarGenerar] = useState(false)
    const [info, guardarInfo] = useState({})

    useEffect(() => {
        // if (generar) {
            if(!identificador) return;
            const busquedaId = async () => {
                const url = `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${identificador}`
                const resultado = await axios.get(url);
                guardarInfo(resultado.data.drinks[0])
            }
            busquedaId()
        // }
    }, [identificador])
    return (
        <ModalContext.Provider
            value={{
                info,
                guardarIdentificador,
                guardarInfo
                // guardarGenerar
            }}
        >
            {props.children}
        </ModalContext.Provider>
    );
}

export default ModalProvider;