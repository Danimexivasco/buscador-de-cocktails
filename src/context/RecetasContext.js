import React, {createContext, useState, useEffect} from 'react';
import axios from 'axios';

export const RecetasContext = createContext();

const RecetasProvider = (props) => {

    const [recetas, guardarRecetas] = useState([]);
    const [busqueda, buscarRecetas] = useState({
        ingrediente:'',
        categoria: ''
    })

    const [consulta, guardarConsulta] = useState(false)

    
    useEffect(() => {
        if(consulta){
            const recetasAPI = async () =>{
                const {ingrediente, categoria} = busqueda;
                const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${ingrediente}&c=${categoria}`
                const recetas = await axios.get(url);
                guardarRecetas(recetas.data.drinks);
            }
            recetasAPI()
        }
        
    }, [busqueda, consulta])

    return ( 
        <RecetasContext.Provider
            value={{
                recetas,
                buscarRecetas,
                guardarConsulta,
                
            }}
        >
            {props.children}
        </RecetasContext.Provider>
     );
}
 
export default RecetasProvider;