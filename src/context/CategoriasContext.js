import React, {createContext, useState, useEffect} from 'react';
import axios from 'axios';

// Creamos el CONTEXT
export const CategoriasContext = createContext();

// Creamos el PROVIDER (Funciones y State)
const CategoriasProvider = (props) =>{

    // Creamos el State del context
    const [categorias, guardarCategorias] = useState([])

    useEffect(()=>{
        const llamadaAPI = async () =>{
            const url = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list'
            const categorias = await axios.get(url)
            guardarCategorias(categorias.data.drinks)
            // console.log(categorias)
        }
        llamadaAPI()
    },[])

    return(
        <CategoriasContext.Provider
            value={{
                categorias
            }}
        >
            {props.children}
        </CategoriasContext.Provider>

    )
}
export default CategoriasProvider;