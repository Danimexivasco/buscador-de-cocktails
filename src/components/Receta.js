import React, { Fragment,useContext, useState } from 'react';
import {ModalContext} from '../context/ModalContext'
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';


function getModalStyle() {
    const top = 50 ;
    const left = 50;
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles(theme => ({
    paper: {
      position: 'absolute',
      width: 450,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
}));

const Receta = ({ receta }) => {

    // Configuracion del MODAL de material-ui
    const [modalStyle] = useState(getModalStyle);
    const [open, setOpen] = useState(false);

    const classes = useStyles();

    const handleOpen = () =>{
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
    }

    const {info, guardarIdentificador, guardarInfo} = useContext(ModalContext);
    
    const MostrarIngredientes = info =>{
        let ingredientes = []
        for (let i = 1; i < 16; i++) {
            if(info[`strIngredient${i}`]){
                ingredientes.push(
                <li key={`strIngredient${i}`}>{info[`strIngredient${i}`]} <i>({info[`strMeasure${i}`]})</i></li>
                )
            }
        }
        return ingredientes;
    }

    return (
        <Fragment>
            <div className="col-md-4 mb-3">
                <div className="card">
                    <h3 className="card-header text-center">{receta.strDrink}</h3>
                    <img className="card-img-top" src={receta.strDrinkThumb} alt={`Imagen de ${receta.strDrink}`} ></img>
                    <div className="card-body">
                        <button
                            type="button"
                            className="btn btn-block btn-primary"
                            onClick={() =>{ 
                                guardarIdentificador(receta.idDrink)
                                // guardarGenerar(true)
                                handleOpen();
                                }}
                        >Ver Cocktail</button>
                        <Modal
                            open = {open}
                            onClose={() =>{
                                guardarIdentificador(null)
                                guardarInfo({})
                                handleClose();
                            } }
                        >
                            <div style={modalStyle} className={classes.paper}>
                                    <h2>{info.strDrink}</h2>
                                    <h3 className="mt-4">Instrucciones</h3>
                                    <p>
                                        {info.strInstructions}
                                    </p>
                                    <img className="img-fluid my-4" src={info.strDrinkThumb} alt={`imagen de info.strDrink`}/>
                                    <h3>Ingredientes y Cantidades</h3>
                                    <ul>
                                        {MostrarIngredientes(info)}
                                    </ul>
                            </div>
                        </Modal>
                    </div>
                </div>

            </div>
        </Fragment>
    );
}

export default Receta;