import React, { useContext, useState } from 'react';
import { CategoriasContext } from '../context/CategoriasContext'
import { RecetasContext } from '../context/RecetasContext';

const Formulario = () => {

    const { categorias } = useContext(CategoriasContext);
    const { buscarRecetas, guardarConsulta } = useContext(RecetasContext);
    const [error, guardarError] = useState(false)

    const [busqueda, guardarBusqueda] = useState({
        ingrediente: '',
        categoria: ''
    })

    const {ingrediente, categoria} = busqueda;

    const leerDatosReceta = e => {
        guardarBusqueda({
            ...busqueda,
            [e.target.name]: e.target.value
        })
    }

    // const buscarReceta = e => {
        
    // }

    return (
        <form
            className="col-12"
            onSubmit={e => {
                e.preventDefault();
                if(ingrediente.trim() === "" || categoria.trim() === ""){
                    guardarError(true)
                    return
                }
                guardarError(false)
                buscarRecetas(busqueda);
                guardarConsulta(true);
            }}
        >

            <fieldset className="text-center">
                <legend>Busca por Categoría o Ingrediente</legend>
            </fieldset>
            {error?<div className="alert alert-danger text-center p-2 col-md-12">Todos los campos son obligatorios</div>:null}
            <div className="row mt-4">
                <div className="col-md-4 mb-4">
                    <input
                        type="text"
                        className="form-control"
                        name="ingrediente"
                        placeholder="Introduce Ingrediente"
                        onChange={leerDatosReceta}
                        value={ingrediente}
                    />
                </div>
                <div className="col-md-4 mb-4">
                    <select
                        name="categoria"
                        className="form-control"
                        onChange={leerDatosReceta}
                        value={categoria}
                    >
                        <option value=""> -- Selecciona una Categoría -- </option>
                        {categorias.map(categoria => {
                            return (
                                <option value={categoria.strCategory} key={categoria.strCategory}>{categoria.strCategory}</option>
                            )
                        })}
                    </select>
                </div>
                
                <div className="col-md-4">
                    <input
                        type="submit"
                        className="btn btn-block btn-primary"
                        value="Buscar"
                    />
                </div>
            </div>
        </form>
    );
}

export default Formulario;